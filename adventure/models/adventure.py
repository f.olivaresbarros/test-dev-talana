import re
from datetime import date
from django.db import models


# Create your models here.

class VehicleType(models.Model):
    name = models.CharField(max_length=32)
    max_capacity = models.PositiveIntegerField()

    def __str__(self) -> str:
        return self.name


class Vehicle(models.Model):
    name = models.CharField(max_length=32)
    passengers = models.PositiveIntegerField()
    vehicle_type = models.ForeignKey(VehicleType, null=True, on_delete=models.SET_NULL)
    number_plate = models.CharField(max_length=10)

    def __str__(self) -> str:
        return self.name

    def can_start(self) -> bool:
        return self.vehicle_type.max_capacity >= self.passengers

    def get_distribution(self) -> list:
        distribution = [[False, False] for _ in range(int(self.vehicle_type.max_capacity / 2))]

        passenger_less = self.passengers
        for i, row in enumerate(distribution):
            if passenger_less > 0:
                if (self.passengers - passenger_less) % 2 == 0 and passenger_less != 1:
                    distribution[i] = [True, True]
                    passenger_less = passenger_less - 2
                else:
                    distribution[i] = [True, False]
                    passenger_less = passenger_less - 1

        return distribution

def validate_number_plate(plate) -> bool:
    numbre_plate_re = re.compile(r'(^([a-zA-Z]{2})(|[0-9]{2})([0-9]{2})$)|(^([a-zA-Z]{2}[0-9]{3})$)')
    return True if numbre_plate_re.match(plate.replace("-", "")) is not None and plate[2] == '-' and plate[5] == '-' else False


class Journey(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.PROTECT)
    start = models.DateField()
    end = models.DateField(null=True, blank=True)

    def __str__(self) -> str:
        return f"{self.vehicle.name} ({self.start} - {self.end})"

    def is_finished(self) -> bool:
        # Less equal?
        return True if self.end is not None and self.end <= date.today() else False
