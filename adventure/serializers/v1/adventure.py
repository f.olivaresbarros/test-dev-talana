from rest_framework import serializers
from adventure.models.adventure import Journey

class JourneySerializerV1(serializers.Serializer):
    name = serializers.CharField()
    passengers = serializers.IntegerField()


class JourneyStopSerializerV1(serializers.ModelSerializer):
    class Meta:
        model = Journey
        fields = (
            'start',
            'end'
        )

    def validate(self, attrs):
        field_names = [f.name for f in self.Meta.model._meta.get_fields()]
        instance = self.Meta.model(
            **{
                field: value
                for field, value in attrs.items()
                if field in field_names
            }
        )
        instance.clean()
        return attrs