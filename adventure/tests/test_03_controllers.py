import pytest
from django.core import mail
from datetime import date
import json

from adventure import models, notifiers, repositories
from adventure.viewsets.v1 import adventures

from .test_02_usecases import MockJourneyRepository

#########
# Tests #
#########

class TestRepository:
    def test_create_vehicle(self, mocker):
        mocker.patch.object(models.Vehicle.objects, "create")
        repo = repositories.JourneyRepository()
        car = models.VehicleType()
        repo.create_vehicle(name="a", passengers=10, vehicle_type=car)
        assert models.Vehicle.objects.create.called


class TestNotifier:
    def test_send_notification(self, mocker):
        mocker.patch.object(mail, "send_mail")
        notifier = notifiers.Notifier()
        notifier.send_notifications(models.Journey())
        assert mail.send_mail.called


class TestCreateVehicleAPIView:
    def test_create(self, client, mocker):
        vehicle_type = models.VehicleType(name="car")
        mocker.patch.object(
            models.VehicleType.objects, "get", return_value=vehicle_type
        )
        mocker.patch.object(
            models.Vehicle.objects,
            "create",
            return_value=models.Vehicle(
                id=1, name="Kitt", passengers=4, vehicle_type=vehicle_type
            ),
        )
        payload = {"name": "Kitt", "passengers": 4, "vehicle_type": "car"}
        response = client.post("/api/adventure/create-vehicle/", payload)
        assert response.status_code == 201


class TestStartJourneyAPIView:
    def test_api(self, client, mocker):
        mocker.patch.object(
            adventures.StartJourneyAPIViewV1,
            "get_repository",
            return_value=MockJourneyRepository(),
        )

        payload = {"name": "Kitt", "passengers": 2}
        response = client.post("/api/adventure/start/", payload)

        assert response.status_code == 201

    def test_api_fail(self, client, mocker):
        mocker.patch.object(
            adventures.StartJourneyAPIViewV1,
            "get_repository",
            return_value=MockJourneyRepository(),
        )

        payload = {"name": "Kitt", "passengers": 6}
        response = client.post("/api/adventure/start/", payload)

        assert response.status_code == 400


class TestStopJourneyAPIView:
    @pytest.mark.django_db
    def test_stop(self, client, mocker):
        # TODO: Implement an endpoint that makes use of a StopJourney use case
        # and tests it
        mocker.patch.object(
            adventures.StopJourneyAPIViewV1,
            'get_repository',
            return_value=repositories.JourneyRepository()
        )

        repo = repositories.JourneyRepository()
        car = repo.get_or_create_car()
        vehicle = repo.create_vehicle(vehicle_type=car, name="End", passengers=2)
        journey = repo.create_journey(vehicle)

        payload = {
            'start': journey.start.strftime('%Y-%m-%d'),
            'end': date.today().strftime('%Y-%m-%d')
        }
        response = client.put('/api/adventure/stop/{pk}'.format(pk=journey.pk), json.dumps(payload), content_type='application/json')

        assert response.status_code == 200


