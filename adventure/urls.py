from django.urls import path

from adventure.viewsets.v1 import adventures

urlpatterns = [
    path("create-vehicle/", adventures.CreateVehicleAPIViewV1.as_view()),
    path("start/", adventures.StartJourneyAPIViewV1.as_view()),
    path("stop/<pk>", adventures.StopJourneyAPIViewV1.as_view()),
]
